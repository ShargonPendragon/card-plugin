tool
extends EditorPlugin

var ui_control

func _enter_tree():
	ui_control = preload("ui/views/database/database_layout.tscn").instance()
	get_editor_interface().get_editor_viewport().add_child(ui_control)
	make_visible(false)

func _exit_tree():
	if ui_control:
		ui_control.queue_free()

func has_main_screen():
	return true


func make_visible(visible):
	if ui_control:
		ui_control.vidible = visible


func get_plugin_name():
	return "CardPlugin"

func get_plugin_icon():
	return get_editor_interface().get_base_control().get_icon("Node", "EditorIcons")
