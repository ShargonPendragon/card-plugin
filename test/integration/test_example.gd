extends "res://addons/gut/test.gd"

var subject = null

func before_each():
	var paths = preload("res://test/paths.gd").new()
	add_child_autofree(paths)
	subject = partial_double(paths.scripts["example"]).new()
	add_child_autofree(subject)

func test_when_example_is_called():
	assert_eq(1, 1)